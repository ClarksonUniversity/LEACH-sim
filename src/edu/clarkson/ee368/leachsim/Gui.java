package edu.clarkson.ee368.leachsim;

import static edu.clarkson.ee368.leachsim.engine.core.ChApplication.Attribute.getScaledWidth;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import edu.clarkson.ee368.leachsim.ButtonClickHandler.ButtonClickEvent;
import edu.clarkson.ee368.leachsim.Input.MouseInputListener;
import edu.clarkson.ee368.leachsim.engine.core.ChApplication;
import edu.clarkson.ee368.leachsim.objects.Node;

/**
 * Visuals for User Interface
 */
public final class Gui implements Drawable, MouseInputListener {

	private final int TITLE_BAR_HEIGHT = 20;

	private ButtonClickHandler buttonClickHandler;

	@SuppressWarnings("unused")
	private TitleBar titleBar;
	private ExitButton exitButton;
	private ControlPanel controlPanel;


	private class TitleBar implements Drawable {

		/**
		 * Title bar region is along the top of the screen, full width and
		 * height specified by Gui.
		 */
		private final Rectangle region;

		/**
		 * Flag whether the exit button should be illuminated (mouse hover).
		 */
		private boolean isFocused = false;

		private final Color unfocusedColor = new Color(0xFFFFFF);
		private final Color focusedColor = new Color(0x555555);
		
		private TitleBar(int height) {
			region = new Rectangle(0, 0, (int) (getScaledWidth() - height), height);
		}

		@Override
		public void draw(Graphics g) {
			final Color previous = g.getColor();

			if (isFocused) {
				g.setColor(Color.RED);
				g.fillRect(region.x, region.y, region.width, region.height);
			}

			g.setColor(isFocused ? focusedColor : unfocusedColor);
			g.drawLine(0, region.height, (int) getScaledWidth(), region.height);
			g.setColor(previous);
		}

		@SuppressWarnings("unused")
		private boolean overlaps(Point point) {
			return region.contains(point);
		}

	}

	private class ExitButton implements Drawable {

		/**
		 * Actual exit button image (cross) is drawn at the region width less
		 * any insets on all sides, where insets are calculated as the button
		 * width times the following factor.
		 */
		private final double borderPercentage = 0.2;

		/**
		 * Exit button region is a square in the upper-right of the screen whose
		 * sides are length
		 */
		private final Rectangle region;

		/**
		 * Flag whether the exit button should be illuminated (mouse hover).
		 */
		private boolean isFocused = false;

		private final Color unfocusedColor = new Color(0x555555);
		private final Color focusedColor = new Color(0xFFFFFF);

		/** Bounds for "X" mark */
		private int x1, y1, x2, y2;

		/**
		 * Create a new Exit Button (square) with the specified side length.
		 */
		private ExitButton(int size) {
			region = new Rectangle((int) (getScaledWidth() - size + 0.5), 0, size, size);

			calculateBounds(size);
		}

		private void calculateBounds(int size) {
			region.setBounds((int) (getScaledWidth() - size + 0.5) - 1, 1, size, size);

			// bounds for "X" mark
			final double x = region.getX() + (size * borderPercentage);
			final double y = region.getY() + (size * borderPercentage);
			final double w = size * (1 - 2 * borderPercentage);
			final double h = size * (1 - 2 * borderPercentage);
			x1 = (int) (x + 0.5);
			y1 = (int) (y + 0.5);
			x2 = (int) (x + w + 0.5);
			y2 = (int) (y + h + 0.5);
		}

		@Override
		public void draw(Graphics g) {
			final Color previous = g.getColor();

			if (isFocused) {
				g.setColor(Color.RED);
				g.fillRect(region.x, region.y, region.width, region.height);
			}

			g.setColor(isFocused ? focusedColor : unfocusedColor);
			g.drawLine(x1, y1, x2, y2);
			g.drawLine(x1, y2, x2, y1);
			g.setColor(previous);
		}

		private void setFocused(boolean focused) {
			this.isFocused = focused;
		}

		private boolean overlaps(Point point) {
			return region.contains(point);
		}
	}

	private class ControlPanel implements Drawable {

		/** Pixel spacing between control panel components */
		private final int componentSpacing = 5;

		private final String roundCounterBase = " / " + SimData.getInstance().getMaxRounds();

		private final int fontHeight = 16;

		private final Point roundCounterLocation;

		/**
		 * Advance Rounds button region is a square in the upper-right of the
		 * screen whose sides are length
		 */
		private final Rectangle roundsButtonRegion;

		/** Side length of Advance Rounds button (square) */
		private final int roundButtonSize = 30;

		/**
		 * Defines the play/advance rounds symbol inside rounds button.
		 */
		private final Polygon playTriangle;

		/**
		 * Flag whether the button should be illuminated (mouse hover).
		 */
		private boolean roundsButtonIsFocused = false;
		
		private ControlPanel(int x, int y) {

			roundCounterLocation = new Point(x, y + fontHeight);

			roundsButtonRegion = new Rectangle(x, y + fontHeight + componentSpacing, roundButtonSize, roundButtonSize);

			// Create play/advance rounds triangle
			{
				final double borderPercentage = 0.2;

				final double x1 = roundsButtonRegion.getX() + (roundsButtonRegion.getWidth() * borderPercentage);
				final double y1 = roundsButtonRegion.getY() + (roundsButtonRegion.getHeight() * borderPercentage);
				final double x2 = x1;
				final double y2 = roundsButtonRegion.getMaxY() - (roundsButtonRegion.getHeight() * borderPercentage);
				final double x3 = roundsButtonRegion.getMaxX() - (roundsButtonRegion.getWidth() * borderPercentage);
				final double y3 = (y1 + y2) / 2.0;
				
				final int[] xPts = new int[] { (int) (x1 + 0.5), (int) (x2 + 0.5), (int) (x3 + 0.5) };
				final int[] yPts = new int[] { (int) (y1 + 0.5), (int) (y2 + 0.5), (int) (y3 + 0.5) };
				playTriangle = new Polygon(xPts, yPts, xPts.length);
			}
		}

		@Override
		public void draw(Graphics g) {

			g.setColor(Color.BLACK);
			final String roundsText = SimData.getInstance().getCurrentRound() + roundCounterBase;
			g.drawString(roundsText, roundCounterLocation.x, roundCounterLocation.y);

			final boolean roundsRemaining = SimData.getInstance().getCurrentRound() < SimData.getInstance()
			        .getMaxRounds();

			if (roundsRemaining) {
				g.setColor(roundsButtonIsFocused ? Color.GREEN : Color.LIGHT_GRAY);
			} else {
				g.setColor(new Color(0xCCCCCC));
			}
			g.fill3DRect(roundsButtonRegion.x, roundsButtonRegion.y, roundsButtonRegion.width,
			        roundsButtonRegion.height, true);

			if (roundsRemaining) {
				g.setColor(roundsButtonIsFocused ? new Color(0xFFFFFF) : new Color(0x555555));
			} else {
				g.setColor(new Color(0xAAAAAA));
			}
			g.fillPolygon(playTriangle);

			if (ChApplication.Flag.verbose) {
				g.setColor(Color.BLACK);

				// Mode
				printDebugLine(g, 1, SimData.getInstance().getMode().toUpperCase());
				// p value
				printDebugLine(g, 2, "p value: " + SimData.getInstance().getPValue());
				// Threshold
				printDebugLine(g, 3, "Threshold: " + ((int) (1E4 * SimData.getInstance().getRoundThreshold())) / 1E4);
				// Number of Cluster Heads
				printDebugLine(g, 4,
				        "Num CH: " + SimData.getInstance().getNodes().stream().filter(Node::isClusterHead).count());
			}
		}

		private void printDebugLine(Graphics g, int line, String text) {
			g.drawString(text, (int) roundsButtonRegion.getX(),
			        (int) (roundsButtonRegion.getMaxY() + line * fontHeight + line * componentSpacing));
		}

		private void setFocused(boolean focused) {
			this.roundsButtonIsFocused = focused;
		}

		private boolean overlapsRounds(Point point) {
			return roundsButtonRegion.contains(point);
		}

	}

	Gui(ButtonClickHandler buttonClickHandler) {

		this.buttonClickHandler = buttonClickHandler;

		titleBar = new TitleBar(TITLE_BAR_HEIGHT);
		exitButton = new ExitButton(TITLE_BAR_HEIGHT);
		controlPanel = new ControlPanel(20, TITLE_BAR_HEIGHT + 20);
	}

	private boolean isFocused() {
		return exitButton.isFocused || controlPanel.roundsButtonIsFocused;
	}

	private boolean onMouseButton(MouseEvent e) {
		boolean processed = false;
		// button click events
		if (e.getButton() == MouseEvent.BUTTON1) {
			if (exitButton.overlaps(e.getPoint())) {
				buttonClickHandler.buttonClicked(new ButtonClickEvent(SimData.BUTTON_EXIT));
				processed = true;
			}

			if (controlPanel.overlapsRounds(e.getPoint())) {
				buttonClickHandler.buttonClicked(new ButtonClickEvent(SimData.BUTTON_ADV_ROUND));
				processed = true;
			}
		}
		return processed;
	}

	/** Drawable */
	@Override
	public void draw(Graphics g) {

		controlPanel.draw(g);

		// titleBar.draw(g); // not fully implemented
		exitButton.draw(g);
	}

	@Override
	public boolean mouseClicked(MouseEvent e) {
		return onMouseButton(e);
	}

	@Override
	public boolean mousePressed(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseReleased(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseEntered(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseExited(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseMoved(MouseEvent e) {
		// button hover events
		exitButton.setFocused(exitButton.overlaps(e.getPoint()));
		controlPanel.setFocused(controlPanel.overlapsRounds(e.getPoint()));
		return exitButton.isFocused || controlPanel.roundsButtonIsFocused;
	}

	@Override
	public boolean mouseDragged(MouseEvent e) {
		return isFocused();
	}

	@Override
	public boolean mouseWheelMoved(MouseWheelEvent e) {
		return isFocused();
	}
}

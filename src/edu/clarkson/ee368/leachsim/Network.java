package edu.clarkson.ee368.leachsim;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.NoninvertibleTransformException;

import edu.clarkson.ee368.leachsim.Input.MouseInputListener;
import edu.clarkson.ee368.leachsim.objects.Node;
import edu.clarkson.ee368.leachsim.protocols.Leach;
import edu.clarkson.ee368.leachsim.protocols.LeachR;
import edu.clarkson.ee368.leachsim.protocols.Protocol;

public class Network implements Drawable, MouseInputListener {

	private SimData data;
	private Protocol protocol;

	private Point mouseXY;
	private Point worldXY = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);

	public Network() {
		data = SimData.getInstance();
		protocol = data.getMode().equals(SimData.MODE_LEACH_R) ? new LeachR() : new Leach();
	}

	public void update() {
		if (data.isReadyAdvanceRound()) {
			data.advanceRound();
			protocol.update();
		}

		// map mouse screen coordinates to world coordinates
		if (mouseXY != null) {
			try {
				worldXY.setLocation(Camera.transformPoint(mouseXY));
			} catch (NoninvertibleTransformException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void draw(Graphics g) {

		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// draw edges
		if (data.getCurrentRound() > 0) {
			Node currentNode = null;
			for (Node node : data.getNodes()) {
				// draw edge between selected node and parent, and its parent,
				// etc. until base
				if (node.overlaps(worldXY)) {
					currentNode = node;
					while (currentNode.getType() != Node.TYPE_BASE) {
						drawEdge(g, currentNode.getLocation(), currentNode.getParent().getLocation());
						currentNode = currentNode.getParent();
					}
				} else if (node.getParent().overlaps(worldXY)) {
					drawEdge(g, node.getLocation(), node.getParent().getLocation());
				}
			}
		}

		// draw nodes
		for (Node node : data.getNodes()) {
			node.draw(g2);
		}
	}

	private void drawEdge(Graphics g, Point p1, Point p2) {
		g.drawLine(p1.x, p1.y, p2.x, p2.y);
	}

	@Override
	public boolean mouseClicked(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mouseEntered(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mouseExited(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mousePressed(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mouseReleased(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mouseMoved(MouseEvent event) {
		mouseXY = event.getPoint();
		return false;
	}

	@Override
	public boolean mouseDragged(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mouseWheelMoved(MouseWheelEvent event) {
		return false;
	}
}

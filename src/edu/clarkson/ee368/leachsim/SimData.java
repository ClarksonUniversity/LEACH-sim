package edu.clarkson.ee368.leachsim;

import java.awt.Color;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import edu.clarkson.ee368.leachsim.objects.Node;

public class SimData {

	private static SimData instance;

	private static Stream<String> inputFileStream;
	private static boolean pregeneratedNodes;

	public static final String BUTTON_EXIT = "exit";
	public static final String BUTTON_ADV_ROUND = "advRound";

	public static final String MODE_LEACH = "LEACH".toLowerCase();
	public static final String MODE_LEACH_R = "LEACH-R".toLowerCase();

	private static ArrayList<Node> nodes;
	private HashMap<Node, Color> nodeColors = new HashMap<>();

	private static String mode = "";
	private static double p = -1.0;
	private double roundThreshold = 0;

	private static int rounds = 0;
	private int currentRound = 0;

	private boolean readyToAdvanceRound = false;

	public static SimData getInstance() {
		if (instance == null) {
			instance = new SimData();
		}
		if (!instance.isValid()) {
			System.err.println("Invalid simulator state.");
			System.exit(0);
		}
		return instance;
	}

	public static void initialize(Stream<String> inputFileStream, ArrayList<Node> nodes, String mode, double pValue,
	        int rounds) {
		SimData.inputFileStream = inputFileStream;
		pregeneratedNodes = !nodes.isEmpty();
		SimData.nodes = nodes;
		SimData.mode = mode;
		SimData.p = pValue;
		SimData.rounds = rounds;
	}

	private SimData() {
		parseInputFile();
		generateNodeColors();
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public Map<Node, Color> getNodeColors() {
		return nodeColors;
	}

	public String getMode() {
		return mode;
	}

	public double getPValue() {
		return p;
	}

	public double getRoundThreshold() {
		return roundThreshold;
	}

	public int getMaxRounds() {
		return rounds;
	}

	public int getCurrentRound() {
		return currentRound;
	}

	public void setRoundThreshold(double threshold) {
		roundThreshold = threshold;
	}

	public void markReadyAdvanceRound() {
		readyToAdvanceRound = true;
	}

	public boolean isReadyAdvanceRound() {
		return readyToAdvanceRound;
	}

	public void advanceRound() {
		currentRound++;
		readyToAdvanceRound = false;
		if (currentRound == rounds) {
			writeOutput();
		}
	}

	public boolean isValid() {

		// ensure valid mode
		boolean validMode = mode != null && (mode.equals(MODE_LEACH) || mode.equals(MODE_LEACH_R));

		// ensure node list has only one base station and at least one other
		// node eligible to be a cluster head
		boolean validNodes = false;
		int countBaseStation = 0;
		int countEligibleNodes = 0;
		if (!nodes.isEmpty()) {
			for (Node node : nodes) {
				if (node.getType() == Node.TYPE_BASE) {
					countBaseStation++;
				} else {
					// LEACH-R requires eligible nodes to be hardened
					if (mode.equals(MODE_LEACH_R) && node.getType() == Node.TYPE_HARDENED) {
						countEligibleNodes++;
					} else if (mode.equalsIgnoreCase(MODE_LEACH)) {
						countEligibleNodes++;
					}
				}
			}
		}
		validNodes = countBaseStation == 1 && countEligibleNodes > 0;

		boolean validP = p > 0.0 && p <= 1.0;
		boolean validRounds = rounds > 0;

		boolean validNodeType = getMode().equals(MODE_LEACH);
		if (!validNodeType) { // LEACH-R
			for (Node n : getNodes()) {
				if (n.getType() == Node.TYPE_HARDENED) {
					validNodeType = true;
					break;
				}
			}
		}

		boolean validNumNodes = getNodes().size() > (1 / getPValue());

		return validNodes && validMode && validP && validRounds && validNodeType && validNumNodes;
	}

	private void parseInputFile() {
		if (inputFileStream == null) {
			System.err.println("Error reading input file.");
			System.exit(0);
		}

		// Read input file into attribute-value pairs
		final String DELIMITER = ":";
		final String COMMENT = "#";
		java.util.List<String[]> pairs = inputFileStream.map(String::trim).map(String::toLowerCase)
		        // keep lines with attribute-value pairs
		        .filter(line -> !line.startsWith(COMMENT) && line.contains(DELIMITER))
		        // strip comments trailing code
		        .map(line -> line.substring(0, line.indexOf(COMMENT) > 0 ? line.indexOf(COMMENT) : line.length()))
		        // split attributes and values
		        .map(s -> s.split(DELIMITER, 2))
		        // skip attribute-value pair if either is empty
		        .filter(pair -> !(pair[0].isEmpty() || pair[1].isEmpty())).collect(Collectors.toList());

		// Process attribute-value pairs
		String attribute, value;
		for (String[] pair : pairs) {
			attribute = pair[0];
			value = pair[1];

			boolean invalidValue = false;
			String valueStatusMessage = "";
			switch (attribute) {
				case "node": {
					if (pregeneratedNodes) {
						break;
					}
					// parse values x,y,t
					int x, y, t;
					String[] values = value.split(",", 3);

					valueStatusMessage = "x,y must be integers";
					try {
						x = Integer.parseInt(values[0].trim());
						y = Integer.parseInt(values[1].trim());
					} catch (NumberFormatException exception) {
						invalidValue = true;
						break;
					}

					valueStatusMessage = "type t must be 0 (base station), 1 (standard), or 2 (hardened/radiation resistant)";
					try {
						t = Integer.parseInt(values[2].trim());
					} catch (NumberFormatException exception) {
						invalidValue = true;
						break;
					}
					invalidValue = !(t == 0 || t == 1 || t == 2);

					nodes.add(new Node(x, y, t));
					break;
				}
				case "mode": {
					if (!mode.isEmpty()) {
						break;
					}
					// parse sim mode string
					valueStatusMessage = "set of possible values are \"" + MODE_LEACH + "\" or \"" + MODE_LEACH_R
					        + "\"";
					invalidValue = !(value.equals(MODE_LEACH) || value.equals(MODE_LEACH_R));
					SimData.mode = value;
					break;
				}
				case "p": {
					if (p >= 0.0) {
						break;
					}
					// parse p value
					double p;
					valueStatusMessage = "value must be defined on the interval (0,1]";
					try {
						p = Double.parseDouble(value);
					} catch (NumberFormatException exception) {
						invalidValue = true;
						break;
					}
					invalidValue = !(p > 0.0 && p <= 1.0);
					SimData.p = p;
					break;
				}
				case "rounds": {
					if (rounds > 0) {
						break;
					}
					// parse rounds value
					int rounds;
					valueStatusMessage = "value must be greater than zero";
					try {
						rounds = Integer.parseInt(value);
					} catch (NumberFormatException exception) {
						invalidValue = true;
						break;
					}
					invalidValue = rounds <= 0;
					SimData.rounds = rounds;
					break;
				}
				default:
					System.err.println("Invalid input file parameter: " + attribute);
					System.exit(0);
			}

			if (invalidValue) {
				System.err.println("Invalid input file format\nattribute: \"" + attribute + "\"\nvalue: \"" + value
				        + "\"\n" + valueStatusMessage);
				System.exit(0);
			}
		}
	}

	private void generateNodeColors() {
		for (Node n : getNodes()) {
			float hue = (float) Math.random();
			float saturation = (float) Math.random() * 0.5f + 0.5f;
			float brightness = (float) Math.random() * 0.3f + 0.7f;
			nodeColors.put(n, new Color(Color.HSBtoRGB(hue, saturation, brightness)));
		}

	}

	public void writeOutput() {

		boolean error = false;
		PrintWriter writer = null;
		String outputFile = "output-" + new Date().getTime() + ".txt";
		try {
			writer = new PrintWriter(new File(outputFile));

			double experimentalPValue = currentRound == 0 ? 0 : Node.getTotalNumberClusterHeads() / (double) currentRound;

			// format output file
			writer.println("Output File");
			writer.println(new Date().toString());
			writer.println();
			writer.println("mode:" + mode.toUpperCase());
			writer.println("p:" + p);
			writer.println("rounds:" + rounds);
			writer.println();
			writer.println("1/p:" + ((int) (1 / p)));
			writer.println("Avg CH round yield:" + experimentalPValue);
			writer.println();
			writer.println("Node data: x, y, t\tRounds as CH");
			writer.println("-----------------------------------------");
			for (Node node : nodes) {
				double percentRoundsAsClusterHead = currentRound == 0 ? 0
				        : node.getNumRoundsAsClusterHead() / (double) currentRound;
				String intrinsicParams = String.format("%1$7s%2$7s%3$2s", ((int) node.getLocation().getX()) + ",",
				        ((int) node.getLocation().getY()) + ",", node.getType());
				writer.println("node:" + intrinsicParams + "    " + node.getNumRoundsAsClusterHead() + " / "
				        + currentRound + " (" + percentRoundsAsClusterHead + ")");
			}

			error = writer.checkError();
			writer.close();

		} catch (Exception e) {
			error = true;
			System.err.println(e.getMessage());
		}

		if (error) {
			System.err.println("Error writing output file");
		} else {
			System.out.println("Output written to " + outputFile);
		}

	}

}
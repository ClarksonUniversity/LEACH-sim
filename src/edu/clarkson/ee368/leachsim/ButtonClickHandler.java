package edu.clarkson.ee368.leachsim;

public interface ButtonClickHandler {

	public class ButtonClickEvent {

		public final String source;

		ButtonClickEvent(String source) {
			this.source = source;
		}
	}

	public void buttonClicked(ButtonClickEvent event);

}

package edu.clarkson.ee368.leachsim.objects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;

import edu.clarkson.ee368.leachsim.Drawable;
import edu.clarkson.ee368.leachsim.SimData;

public class Node implements Drawable {

	public static final int TYPE_BASE = 0;
	public static final int TYPE_STANDARD = 1;
	public static final int TYPE_HARDENED = 2;

	private static final Color DEFAULT_COLOR = Color.BLACK;
	private static int defaultDiameter = 25;

	private static int totalNumberClusterHeads = 0;

	private static Node base;

	public static void setDefaultDiameter(int diameter) {
		defaultDiameter = diameter;
	}

	public static int getTotalNumberClusterHeads() {
		return totalNumberClusterHeads;
	}

	public static Node getBaseStation() {
		return base;
	}

	private Point point;
	private int type;
	private Color color;
	private int diameter;
	private boolean isClusterHead;

	private Node parent;

	private int numRoundsAsClusterHead;

	/**
	 * Node topology definition
     * where x is an integer x coordinate
     * y is an integer y coordinate
     * t is an integer denoting type (used by LEACH-R algorithm)
     * 		t=0:  standard
     * 		t=1:  hardened/radiation resistant
	 */
	public Node(int x, int y, int t) {
		point = new Point(x, y);
		type = t;
		color = DEFAULT_COLOR;
		diameter = defaultDiameter;
		isClusterHead = false;
		numRoundsAsClusterHead = 0;
		parent = this;

		if (type == TYPE_BASE) {
			base = this;
			diameter = defaultDiameter * 3;
		}
	}

	public boolean overlaps(Point p) {
		return p != null && getLocation().distanceSq(p) < Math.pow(getDiameter() / 2.0, 2);
	}

	public Point getLocation() {
		return point;
	}

	public int getType() {
		return type;
	}

	public Color getColor() {
		return color;
	}

	public int getDiameter() {
		return diameter;
	}

	public Node getParent() {
		return parent;
	}

	public boolean isClusterHead() {
		return isClusterHead;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void setDiameter(int diameter) {
		this.diameter = diameter;
	}

	public void setClusterHead(boolean isClusterHead) {
		this.isClusterHead = isClusterHead;

		if (isClusterHead) {
			numRoundsAsClusterHead++;
			totalNumberClusterHeads++;
			parent = base;
		}
	}
	
	public void setParentNode(Node clusterHead) {
		parent = clusterHead;
	}

	public int getNumRoundsAsClusterHead() {
		return numRoundsAsClusterHead;
	}

	@Override
	public void draw(Graphics g) {

		Graphics2D g2 = (Graphics2D) g;

		int offset = diameter / 2;
		g.setColor(color);

		if (type == TYPE_BASE) {
			g.fillRect(point.x - offset, point.y - offset, diameter, diameter);
		} else {

			g.fillOval(point.x - offset, point.y - offset, diameter, diameter);

			// cluster heads receive thick black outline
			if (isClusterHead
			        || (SimData.getInstance().getMode().equals(SimData.MODE_LEACH_R)) && getType() == TYPE_HARDENED) {

				int border = 2;
				if (isClusterHead) {
					border = (int) (diameter * 0.15) + 3;
				}

				Stroke oldStroke = g2.getStroke();
				g2.setStroke(new BasicStroke(border));
				g.setColor(Color.BLACK);
				g.drawOval(point.x - offset, point.y - offset, diameter, diameter);
				g2.setStroke(oldStroke);
			}
		}
	}

}

package edu.clarkson.ee368.leachsim.protocols;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import edu.clarkson.ee368.leachsim.SimData;
import edu.clarkson.ee368.leachsim.objects.Node;

public class LeachR extends Protocol {

	/**
	 * The LEACH-R Protocol Implementation defines the set of eligible nodes to
	 * contain any node which has not been a Cluster Head since the following
	 * condition was true: r modulo (1/P) = 0, where r is the Round number and P
	 * is the desired percentage of Cluster Heads each round. All nodes become
	 * eligible when r modulo (1/P) = 0 regardless of their history as Cluster
	 * Heads. The LEACH-R Protocol Implementation places an additional
	 * constraint on the eligibility of nodes to be chosen as Cluster Heads
	 * compared to LEACH. LEACH-R uses a heterogeneous network of nodes where
	 * some nodes are “hardened”. Only hardened nodes may be considered for
	 * Cluster Heads.
	 * 
	 */
	@Override
	protected boolean nodeIsEligible(Node n) {
		return (n.getType() == Node.TYPE_HARDENED)
		        && (n.getNumRoundsAsClusterHead() == SimData.getInstance().getCurrentRound()
		                / ((int) (1 / SimData.getInstance().getPValue())));
	}

	@Override
	protected void assignParents() {
		// find shortest path from cluster heads to base station
		new Dijkstra(clusterHeads, Node.getBaseStation());

		// map: cluster heads -> member nodes
		HashMap<Node, ArrayList<Node>> clusters = new HashMap<>();
		for (Node n : data.getNodes()) {
			if (n.getType() == Node.TYPE_BASE || n.isClusterHead()) {
				continue;
			}

			if (clusterHeads.contains(n.getParent())) {

				if (clusters.containsKey(n.getParent())) {
					// CH already exists in map
					clusters.get(n.getParent()).add(n);
				} else {
					// CH not yet added to map
					ArrayList<Node> cluster = new ArrayList<Node>();
					cluster.add(n);
					clusters.put(n.getParent(), cluster);
				}
			}
		}

		// find shortest path through cluster members to cluster head
		for (Entry<Node, ArrayList<Node>> cluster : clusters.entrySet()) {
			new Dijkstra(cluster.getValue(), cluster.getKey());
		}
	}

	private class Dijkstra {

		private List<Node> nodes;
		private double[] distToBase;

		private double[][] matrix;

		private Dijkstra(List<Node> nodes, Node base) {
			this.nodes = nodes;
			distToBase = new double[nodes.size()];
			matrix = new double[nodes.size()][nodes.size()];
			for (int r = 0; r < nodes.size(); r++) {
				distToBase[r] = nodes.get(r).getLocation().distanceSq(base.getLocation());
				for (int c = 0; c < nodes.size(); c++) {
					if (r == c) {
						continue;
					}
					matrix[r][c] = nodes.get(c).getLocation().distanceSq(nodes.get(r).getLocation());
				}
			}
			assignShortestPath();
		}

		private void assignShortestPath() {
			for (int r = 0; r < nodes.size(); r++) {
				for (int c = 0; c < nodes.size(); c++) {
					if (distToBase[c] > matrix[r][c] + distToBase[r]) {
						distToBase[c] = matrix[r][c] + distToBase[r];
						nodes.get(c).setParentNode(nodes.get(r));
					}
				}
			}
		}
	}
}

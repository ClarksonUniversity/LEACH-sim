package edu.clarkson.ee368.leachsim.protocols;

import java.util.ArrayList;
import java.util.List;

import edu.clarkson.ee368.leachsim.SimData;
import edu.clarkson.ee368.leachsim.objects.Node;

public abstract class Protocol {

	protected SimData data;
	protected List<Node> clusterHeads;

	public Protocol() {
		data = SimData.getInstance();
		clusterHeads = new ArrayList<Node>();
	}

	public void update() {

		boolean clusteringInProgess = true;
		while (clusteringInProgess) {
			data.setRoundThreshold(clusteringThreshold(data.getCurrentRound(), data.getPValue()));
			clusteringInProgess = !selectClusterHeads(data.getRoundThreshold());
			if (clusteringInProgess) {
				data.advanceRound();
			}
		}
		assignClusterMembers();

		assignClusterColorScheme();

		assignParents();

	}

	private double clusteringThreshold(int currentRound, double p) {
		return (p / (1 - p * (currentRound % (int) (1 / p))));
	}

	/** true when at least one cluster head was chosen */
	private boolean selectClusterHeads(double threshold) {
		clusterHeads.clear();
		for (Node n : data.getNodes()) {
			if (commonEligibility(n)) {
				if (Math.random() < threshold) {
					n.setClusterHead(true);
					clusterHeads.add(n);
				} else {
					n.setClusterHead(false);
				}
			} else {
				n.setClusterHead(false);
			}
		}
		return !clusterHeads.isEmpty();
	}

	private void assignClusterMembers() {
		for (Node n : data.getNodes()) {

			// Skip cluster heads
			if (clusterHeads.contains(n)) {
				continue;
			}

			// Assign node to the nearest cluster head
			double minDist = Double.MAX_VALUE;
			Node nearestClusterHead = clusterHeads.get(0);
			for (Node ch : clusterHeads) {
				double dist = n.getLocation().distanceSq(ch.getLocation());
				if (dist < minDist) {
					minDist = dist;
					nearestClusterHead = ch;
				}
			}
			n.setParentNode(nearestClusterHead);
		}
	}

	private void assignClusterColorScheme() {
		for (Node node : clusterHeads) {
			node.setColor(data.getNodeColors().get(node));
		}
		for (Node n : data.getNodes()) {

			// Skip base station
			if (n.getType() == Node.TYPE_BASE) {
				continue;
			} else if (!n.isClusterHead()) {
				n.setColor(n.getParent().getColor());
			}
		}

	}

	private boolean commonEligibility(Node n) {
		if (n.getType() == Node.TYPE_BASE) {
			return false;
		} else {
			return nodeIsEligible(n);
		}
	}

	protected abstract boolean nodeIsEligible(Node n);

	protected abstract void assignParents();

}

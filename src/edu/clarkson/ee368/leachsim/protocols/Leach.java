package edu.clarkson.ee368.leachsim.protocols;

import edu.clarkson.ee368.leachsim.SimData;
import edu.clarkson.ee368.leachsim.objects.Node;

public class Leach extends Protocol {

	/**
	 * The LEACH Protocol Implementation defines the set of eligible nodes to
	 * contain any node which has not been a Cluster Head since the following
	 * condition was true: r modulo (1/P) = 0, where r is the Round number and P
	 * is the desired percentage of Cluster Heads each round. All nodes become
	 * eligible when r modulo (1/P) = 0 regardless of their history as Cluster
	 * Heads.
	 */
	@Override
	protected boolean nodeIsEligible(Node n) {
		return n.getNumRoundsAsClusterHead() == SimData.getInstance().getCurrentRound()
		        / ((int) (1 / SimData.getInstance().getPValue()));
	}

	@Override
	protected void assignParents() {
	}

}

package edu.clarkson.ee368.leachsim;

public interface Drawable {

	void draw(java.awt.Graphics g);

}

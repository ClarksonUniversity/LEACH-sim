package edu.clarkson.ee368.leachsim;

import java.awt.Color;
import java.awt.Graphics;

import edu.clarkson.ee368.leachsim.engine.core.ChApplicationAdapter;

public final class Simulator extends ChApplicationAdapter implements ButtonClickHandler {

	private static final long serialVersionUID = 1L;

	private Input input;
	private Gui gui;
	private Network network;
	private Camera camera;

	protected void init() {

		input = new Input(this);

		gui = new Gui(this);
		network = new Network();
		camera = new Camera(this);

		// gui receives events before camera
		input.addMouseInputListener(gui);
		input.addMouseInputListener(camera);
		input.addMouseInputListener(network);

		Flag.visible = true;
	}

	protected void update() {
		network.update();
	}

	protected void draw(Graphics g) {

		// project world space to screen space using current pan and zoom
		camera.setProjection(g);

		// draw network topology
		network.draw(g);

		// unproject for UI elements
		camera.resetProjection(g);
		gui.draw(g);

		// window border
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, (int) Attribute.getScaledWidth() - 1, (int) Attribute.getScaledHeight() - 1);
	}

	@Override
	public void buttonClicked(ButtonClickEvent event) {
		switch (event.source) {
			case SimData.BUTTON_EXIT:
				stop();
				break;
			case SimData.BUTTON_ADV_ROUND:
				if (SimData.getInstance().getCurrentRound() < SimData.getInstance().getMaxRounds()) {
					SimData.getInstance().markReadyAdvanceRound();
				}
				break;
			default:
				// do nothing
		}
	}

}
